USE CASE
---------
Payment gateway.
You need this module if you are using Sage Payment Solutions (North America) 
to accept and process payments through Drupal Commerce.
If you are using Sage Pay (Europe) use https://drupal.org/project/commerce_sagepay instead.


USAGE & NOTES
-------------
Configure at /admin/commerce/config/sagepayments

Give appropriate admin users permission to 'Administer Sage Payments'.

This module supports Sage Payments' 'Sale' and 'Authorization' transaction types.

This module sends only the information required by Sage Payments.
If you need to send detailed transaction information from Drupal to Sage Payments, 
you can configure in commerce_sage_payments_charge in commerce_sage_payments.module.
Detailed shipping information would require setting up a customer shipping profile through commerce.

With a little work, the 'Force' transaction type could be made to work.


ATTRIBUTION
-----------
Commerce port of https://drupal.org/project/uc_sage_payments


SPONSORSHIP
-----------
Sponsored by Pixeldust Interactive.
http://www.pixeldust.net/